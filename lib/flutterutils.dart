library flutterutils;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:focus_widget/focus_widget.dart';

double porcentagemLargura(BuildContext context, double valor1, {BoxConstraints? constraints}) {
  double a = (constraints?.maxWidth ?? MediaQuery.of(context).size.width) / 100.0 * valor1;
  return a;
}

double porcentagemAltura(BuildContext context, double valor1, {BoxConstraints? constraints}) {
  double a = (constraints?.maxHeight ?? MediaQuery.of(context).size.height) / 100.0 * valor1;
  return a;
}

double porcentagemMaiorLado(BuildContext context, double valor1, {BoxConstraints? constraints}) {
  double height = constraints?.maxHeight ?? MediaQuery.of(context).size.height;
  double width = constraints?.maxWidth ?? MediaQuery.of(context).size.width;
  if (width > height) {
    double a = width / 100.0 * valor1;
    return a;
  } else {
    double a = height / 100.0 * valor1;
    return a;
  }
}

double porcentagemMenorLado(BuildContext context, double valor1, {BoxConstraints? constraints}) {
  double height = constraints?.maxHeight ?? MediaQuery.of(context).size.height;
  double width = constraints?.maxWidth ?? MediaQuery.of(context).size.width;
  if (width < height) {
    double a = width / 100.0 * valor1;
    return a;
  } else {
    double a = height / 100.0 * valor1;
    return a;
  }
}

double porcentagemMediaDosLados(BuildContext context, double valor1, {BoxConstraints? constraints}) {
  double height = constraints?.maxHeight ?? MediaQuery.of(context).size.height;
  double width = constraints?.maxWidth ?? MediaQuery.of(context).size.width;
  double a = (width + height) / 2.0;
  a = a / 100.0 * valor1;
  return a;
}

double tamanhoTexto(BuildContext context, double valor1, {BoxConstraints? constraints}) {
  double width = constraints?.maxWidth ?? MediaQuery.of(context).size.width;
  if (width > 300) {
    return valor1;
  } else {
    return 12;
  }
}

int divisaoLargura(BuildContext context, double valor1, {BoxConstraints? constraints}) {
  double a = (constraints?.maxWidth ?? MediaQuery.of(context).size.width) / valor1;
  return a.truncate();
}

double col_1(BuildContext context) {
  final width = 8.33 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_2(BuildContext context) {
  final width = 16.66 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_3(BuildContext context) {
  final width = 25 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_4(BuildContext context) {
  final width = 33.33 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_5(BuildContext context) {
  final width = 41.66 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_6(BuildContext context) {
  final width = 50 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_7(BuildContext context) {
  final width = 58.33 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_8(BuildContext context) {
  final width = 66.66 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_9(BuildContext context) {
  final width = 75 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_10(BuildContext context) {
  final width = 83.33 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_11(BuildContext context) {
  final width = 91.66 / 100 * MediaQuery.of(context).size.width;
  return width;
}

double col_12(BuildContext context) {
  final width = 100 / 100 * MediaQuery.of(context).size.width;
  return width;
}

final double mobileSize = 576.0;
final double tabletSize = 768.0;
final double desktopLgSize = 992.0;
final double desktopXlSize = 1280.0;
final double desktopXXlSize = 1400.0;
final Map<String, double> screeenSizes = {
  "mobile": mobileSize,
  "tablet": tabletSize,
  "desktop1": desktopLgSize,
  "desktop2": desktopXlSize,
  "desktop3": desktopXXlSize,
};

double closestSize(Map<String, double> map, BuildContext context) {
  var width = MediaQuery.of(context).size.width;
  double? minDiff;
  String? closestKey;

  for (var key in map.keys) {
    double screeenSize = screeenSizes[key]!;
    final diff = (width - screeenSize).abs();
    if (minDiff == null || diff < minDiff) {
      minDiff = diff;
      closestKey = key;
    }
  }
  return map[closestKey]!;
}

responsiveSize(BuildContext context, {mobile, tablet, desktop1, desktop2, desktop3}) {
  final Map<String, double?> sizes = {
    "mobile": mobile,
    "tablet": tablet,
    "desktop1": desktop1,
    "desktop2": desktop2,
    "desktop3": desktop3,
  };
  sizes.removeWhere((key, value) => value == null);
  Map<String, double> sizes2 = Map.fromEntries(sizes.entries.map(
    (e) => MapEntry(e.key, e.value!),
  ));

  if (sizes2.isEmpty) {
    throw Exception("At least one size must be provided.");
  }

  if (isMobile(context)) {
    return sizes2["mobile"] ?? closestSize(sizes2, context);
  } else if (isTablet(context)) {
    return sizes2["tablet"] ?? closestSize(sizes2, context);
  } else if (isDesktopLg(context)) {
    return sizes2["desktop1"] ?? closestSize(sizes2, context);
  } else if (isDesktopXl(context)) {
    return sizes2["desktop2"] ?? closestSize(sizes2, context);
  } else if (isDesktopXXl(context)) {
    return sizes2["desktop3"] ?? closestSize(sizes2, context);
  }

  // Fallback to the default size if none of the conditions match
  return sizes2.values.first;
}

bool isMobile(BuildContext context) {
  return MediaQuery.of(context).size.width <= mobileSize;
}

bool isMobileUP(BuildContext context) {
  return MediaQuery.of(context).size.width >= mobileSize;
}

bool isMobileDOWN(BuildContext context) {
  return MediaQuery.of(context).size.width <= mobileSize;
}

bool isTablet(BuildContext context) {
  return MediaQuery.of(context).size.width > mobileSize && MediaQuery.of(context).size.width <= tabletSize;
}

bool isTabletUP(BuildContext context) {
  return MediaQuery.of(context).size.width >= tabletSize;
}

bool isTabletDOWN(BuildContext context) {
  return MediaQuery.of(context).size.width <= tabletSize;
}

bool isDesktopLg(BuildContext context) {
  return MediaQuery.of(context).size.width > tabletSize && MediaQuery.of(context).size.width <= desktopLgSize;
}

bool isDesktopLgUP(BuildContext context) {
  return MediaQuery.of(context).size.width >= desktopLgSize;
}

bool isDesktopLgDOWN(BuildContext context) {
  return MediaQuery.of(context).size.width <= desktopLgSize;
}

bool isDesktopXl(BuildContext context) {
  return MediaQuery.of(context).size.width > desktopLgSize && MediaQuery.of(context).size.width <= desktopXlSize;
}

bool isDesktopXlUP(BuildContext context) {
  return MediaQuery.of(context).size.width >= desktopXlSize;
}

bool isDesktopXlDOWN(BuildContext context) {
  return MediaQuery.of(context).size.width <= desktopXlSize;
}

bool isDesktopXXl(BuildContext context) {
  return MediaQuery.of(context).size.width > desktopXlSize && MediaQuery.of(context).size.width <= desktopXXlSize;
}

bool isDesktopXXlUP(BuildContext context) {
  return MediaQuery.of(context).size.width >= desktopXXlSize;
}

bool isDesktopXXlDOWN(BuildContext context) {
  return MediaQuery.of(context).size.width <= desktopXXlSize;
}

FilteringTextInputFormatter keyBoardDoubleFormat = FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*'));

FilteringTextInputFormatter keyBoardIntegerFormat = FilteringTextInputFormatter.allow(RegExp(r'^[0-9]*'));

FilteringTextInputFormatter keyBoardAlphaNumericFormat = FilteringTextInputFormatter.allow(RegExp(r'^[a-zA-Z0-9]*'));

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    Map<String, dynamic> map = newValue.toJSON();
    map["text"] = map["text"].toString().toUpperCase();
    return TextEditingValue.fromJSON(map);
  }
}

bool stringNulaOuVazia(String? v) {
  if (v == null || v.isEmpty) {
    return true;
  }
  return false;
}

String? defaultValidator(String? v) {
  return stringNulaOuVazia(v) ? "^" : null;
}

String? differentValidator(String? v, String v2, String msg) {
  return v != v2 ? msg : null;
}

String? upperCaseValidator(String? v, {String msg = "Deve ter uma letra maiuscula"}) {
  RegExp regex = RegExp(r'(?=.*[A-Z])');
  return !regex.hasMatch(v!) ? msg : null;
}

String? spetialCharacterValidator(String? v, {String msg = "Deve ter um caractere especial"}) {
  RegExp regex = RegExp(r'(?=.*[\W_])');
  return !regex.hasMatch(v!) ? msg : null;
}

String? numberValidator(String? v, {String msg = "Deve ter um número"}) {
  RegExp regex = RegExp(r'(?=.*[0-9])');
  return !regex.hasMatch(v!) ? msg : null;
}

String? sizeValidator(String? v, int size, {String? msg}) {
  String msg2 = msg ?? "Deve ter $size caracteres";
  return v!.length < size ? msg2 : null;
}

String? regexValidator(String? v, String pattern, String msg) {
  RegExp regex = RegExp(pattern);
  return !regex.hasMatch(v!) ? msg : null;
}

String? processValidations(String? v, List<Function> validations) {
  List<String> list = [];
  for (var obj in validations) {
    String? value = obj.call(v);
    if (value != null) {
      list.add(value);
    }
  }
  if(list.isNotEmpty){
    return list.join("\n");
  }
  return null;
}

Color invertColor(Color color) {
  final r = 255 - color.red;
  final g = 255 - color.green;
  final b = 255 - color.blue;
  return Color.fromARGB((color.opacity * 255).round(), r, g, b);
}

Size textSize(double tamanho) {
  TextStyle textStyle = TextStyle(fontSize: tamanho, height: 0);
  final TextPainter textPainter = TextPainter(text: TextSpan(text: "1", style: textStyle), maxLines: 1, textDirection: TextDirection.ltr)..layout(minWidth: 0, maxWidth: double.infinity);
  return textPainter.size;
}

class MyTextFormField extends StatelessWidget {
  String hint;
  Function initialFunc;
  Function onChanged;
  Function? botaoFunc;
  IconData? botaoIcon;
  var padding;
  Function? onBlur;
  double? width;
  late TextEditingController textController;
  String? Function(String? v)? validator;
  List<TextInputFormatter>? formatacao;
  Function? observerFunc;
  bool obscure;
  Function? ontap;
  bool Function()? readOnly;
  var boder;
  TextStyle? textStyle;
  TextStyle? labelStyle;
  TextStyle? hintStyle;
  var cursorColor;
  var prefixIcon;
  TextInputType? keyboardType;
  FocusNode? focusNode;
  bool Function()? rendered;
  int? maxLines;
  int? minLines;

  MyTextFormField(
    this.hint,
    Function this.initialFunc,
    Function this.onChanged, {
    this.padding,
    this.onBlur,
    this.width,
    textController2,
    this.validator = defaultValidator,
    this.formatacao,
    this.observerFunc,
    this.botaoFunc,
    this.botaoIcon,
    this.obscure = false,
    this.ontap,
    this.readOnly,
    this.boder,
    this.textStyle,
    this.labelStyle,
    this.hintStyle,
    this.cursorColor,
    this.prefixIcon,
    this.keyboardType,
    this.focusNode,
    this.rendered,
    this.maxLines = 1,
    this.minLines,
  }) {
    if (textController2 == null) {
      textController = TextEditingController();
    } else {
      textController = textController2;
    }
    if (onBlur != null) {
      if (focusNode == null) {
        focusNode = FocusNode();
      }
      focusNode!.addListener(() {
        if (!focusNode!.hasFocus) {
          onBlur!.call(textController.text);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      name: hint,
      builder: (context) {
        if (rendered != null) {
          if (!rendered!.call()) {
            return Container();
          }
        }
        String? text = initialFunc.call();
        if (textController.text != text) {
          Future.delayed(Duration.zero, () async {
            if (text != null) {
              textController.text = text;
            } else {
              textController.text = "";
            }
          });
        }
        bool readOnly2 = false;
        if (readOnly != null) {
          readOnly2 = readOnly!.call();
        }
        dynamic textFormField = TextFormField(
          focusNode: focusNode,
          controller: textController,
          inputFormatters: formatacao,
          style: textStyle,
          decoration: InputDecoration(hintText: hint, labelText: hint, hintStyle: hintStyle, border: boder, prefixIcon: prefixIcon, labelStyle: labelStyle),
          cursorColor: cursorColor,
          onChanged: (value) {
            onChanged.call(value);
          },
          validator: validator,
          obscureText: obscure,
          onTap: ontap as void Function()?,
          readOnly: readOnly2,
          keyboardType: keyboardType,
          maxLines: maxLines,
          minLines: minLines,
        );

        if (onBlur != null) {
          textFormField = FocusWidget(
            focusNode: focusNode!,
            child: textFormField,
          );
        }
        if (botaoFunc != null) {
          List<Widget> rowWidgets = [];
          var row = Row(
            children: rowWidgets,
          );
          rowWidgets.add(Expanded(child: textFormField));
          rowWidgets.add(IconButton(
            icon: Icon(botaoIcon ?? Icons.close),
            onPressed: () {
              botaoFunc?.call();
            },
          ));
          textFormField = row;
        }

        return Container(
          width: width,
          padding: padding,
          child: textFormField,
        );
      },
    );
  }
}

class MyDropdownButtonFormField extends StatelessWidget {
  Function init;
  Function onChanged;
  Function? remove;
  List<DropdownMenuItem> itens;
  String hint;

  MyDropdownButtonFormField(this.hint, this.itens, this.init, this.onChanged, {this.remove});

  @override
  Widget build(BuildContext context) {
    var observer = Observer(
      builder: (context) {
        List<Widget> list = [];
        list.add(Expanded(
          child: DropdownButtonFormField(
            value: init.call(),
            decoration: InputDecoration(hintText: hint, labelText: hint),
            items: itens,
            onChanged: (dynamic value) {
              onChanged.call(value);
            },
          ),
        ));
        if (remove != null) {
          list.add(IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              remove?.call();
            },
          ));
        }
        return Row(
          children: list,
        );
      },
    );
    return observer;
  }
}
